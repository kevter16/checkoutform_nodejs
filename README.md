# checkoutform_nodejs

## To run this example you need do the folowing:

1. Download or clone this repo.
2. Create a file named .env with the varables in .env.default or rename it, and add the credentials provided by Greenpay support team.
3. Execute in git terminal npm install
4. Execute in git terminal node checkoutform.js